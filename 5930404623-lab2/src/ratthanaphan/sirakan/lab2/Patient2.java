package ratthanaphan.sirakan.lab2;

public class Patient2 {
	public static void main(String[] args) {
		if(args.length!=4)
		{
			System.err.println("Patient <name> <gender> <weight> <height>");
		}
		else
		{
			System.out.println("This patient name is " + args[0]);
			double weight = (Double.parseDouble(args[2]));
        	double height = (Double.parseDouble(args[3]));
        	if(weight<0)
        	{
        		System.out.println("Weight must be non-negative");
        	}
        	else if(height<0)
        	{
        		System.out.println("Height must be non-negative");
        	}
        	else if(args[1].equalsIgnoreCase("Female"))
        	{
        		System.out.println("Her weight is " + args[2] + " kg. and height is " + args[3] + " cm.");
        	}
        	else if(args[1].equalsIgnoreCase("Male"))
        	{
        		System.out.println("His weight is " + args[2] + " kg. and height is " + args[3] + " cm.");
        	}
        	else
        	{
        		System.out.println("Please enter gender only male and female.");
        	}
		}
        
        
	}
}
