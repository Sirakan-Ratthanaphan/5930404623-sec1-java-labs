/*
 * This program is to sort the integers.
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/01/17
 */
package ratthanaphan.sirakan.lab2;

import java.util.Arrays;

public class IntegersSorter {
	public static void main(String args[]) {
		int numberofintegers = (Integer.parseInt(args[0]));
		int[] integer = new int[numberofintegers];
		for (int i = 0; i < numberofintegers; i++) {
			integer[i] = (Integer.parseInt(args[i + 1]));
		}
		System.out.print("Before sorting: [");
		for (int i = 0; i < numberofintegers; i++) {
			if (i != numberofintegers-1) {
				System.out.print(integer[i] + ", ");
			} else {
				System.out.print(integer[i]);
			}
		}
		System.out.println("]");
		Arrays.sort(integer);
		System.out.print("After sorting: [");
		for (int i = 0; i < numberofintegers; i++) {
			if (i != numberofintegers-1) {
				System.out.print(integer[i] + ", ");
			} else {
				System.out.print(integer[i]);
			}
		}
		System.out.println("]");
	}
}
