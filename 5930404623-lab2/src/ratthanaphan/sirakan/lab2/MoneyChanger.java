/*
 * This program is to compute the money to give an exchange
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/01/17
 */
package ratthanaphan.sirakan.lab2;

public class MoneyChanger {
	public static void main(String[] args) {
		int cost = (Integer.parseInt(args[0]));
		System.out.println("You give 1000 for the cost as " + cost + " Bath.");
		int RecieveExchange = 1000 - cost;
		System.out.println("You will recieve exchange as " + RecieveExchange + "Bath.");
		int bank500, bank100, bank50, bank20;
		cost = 1000 - cost;
		bank500 = cost / 500;
		cost = cost % 500;
		bank100 = cost / 100;
		cost = cost % 100;
		bank50 = cost / 50;
		cost = cost % 50;
		bank20 = cost / 20;
		if (bank500 != 0) {
			System.out.print(bank500 + " of 500 bank note; ");
		}
		if (bank100 != 0) {
			System.out.print(bank100 + "of 100 bank note; ");
		}
		if (bank50 != 0) {
			System.out.print(bank50 + "of 50 bank note; ");
		}
		if (bank20 != 0) {
			System.out.print(bank20 + "of 20 bank note; ");
		}
	}

}
