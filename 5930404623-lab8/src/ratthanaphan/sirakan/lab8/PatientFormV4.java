/**
 * PatientFormV4
 * @author Sirakan Ratthanaphan 593040462-3 sec1
 *         Created on 3/22/2017.
 */
package ratthanaphan.sirakan.lab8;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import ratthanaphan.sirakan.lab6.*;

public class PatientFormV4 extends PatientFormV3 implements ActionListener, ItemListener {

	private static final long serialVersionUID = 1L;

	public PatientFormV4(String titleName) {
		super(titleName);
	}

	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.setFrameFeatures();
		patientForm4.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addListeners() {
		OKButton.addActionListener(this);
		cancelButton.addActionListener(this);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		String messageOutput = "";
		if (command.equals("OK")) {
			messageOutput = "Name = " + nameTextField.getText() + " Birthdate = " + birthdateTextField.getText()
					+ " Weight = " + weightTextField.getText() + " Height = " + heightTextField.getText()
					+ "\nGender = ";

			if (femaleCheckBox.isSelected()) {
				messageOutput += "Female\n";
			} else if (maleCheckBox.isSelected()) {
				messageOutput += "Male\n";
			}
			messageOutput += "Address = " + addressTextArea.getText();
			if (typeOfPatientChoice.getSelectedItem().equals("OutPatient")) {
				messageOutput += "\n Type =  OutPatient";
			} else if (typeOfPatientChoice.getSelectedItem().equals("InPatient")) {
				messageOutput += "\n Type = InPatient";
			}
			JOptionPane.showMessageDialog(this, messageOutput);
		} else if (command.equals("Cancel")) {
			nameTextField.setText("");
			birthdateTextField.setText("");
			weightTextField.setText("");
			heightTextField.setText("");
			addressTextArea.setText("");
		}
	}

}
