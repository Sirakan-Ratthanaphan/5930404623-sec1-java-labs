/**
 * PatientFormV5 
 * @author Sirakan Ratthanaphan 593040462-3 sec1
 *         Created on 3/22/2017.
 */
package ratthanaphan.sirakan.lab8;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PatientFormV5 extends PatientFormV4 implements ActionListener, ItemListener {

	private static final long serialVersionUID = -6682044529657965435L;
	protected JMenu menuColor, menuSize;
	protected JMenuItem subMenuColorRed, subMenuColorBlue, subMenuColorGreen, subMenuColorOther, subMenuSize16,
			subMenuSize20, subMenuSize24, subMenuSizeOther;

	public PatientFormV5(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		menuColor = new JMenu("Color");
		menuSize = new JMenu("Size");
		subMenuColorRed = new JMenuItem("Red");
		subMenuColorBlue = new JMenuItem("Blue");
		subMenuColorGreen = new JMenuItem("Green");
		subMenuColorOther = new JMenuItem("Custom...");
		subMenuSize16 = new JMenuItem("16");
		subMenuSize20 = new JMenuItem("20");
		subMenuSize24 = new JMenuItem("24");
		subMenuSizeOther = new JMenuItem("Custom...");

		configMenu.remove(colorMenuItem);
		configMenu.remove(sizeMenuItem);
		menuColor.add(subMenuColorRed);
		menuColor.add(subMenuColorBlue);
		menuColor.add(subMenuColorGreen);
		menuColor.add(subMenuColorOther);

		menuSize.add(subMenuSize16);
		menuSize.add(subMenuSize20);
		menuSize.add(subMenuSize24);
		menuSize.add(subMenuSizeOther);

		configMenu.add(menuColor);
		configMenu.add(menuSize);

		menuBar.add(configMenu);

	}

	public static void createAndShowGUI() {
		PatientFormV5 patientForm5 = new PatientFormV5("Patient Form V5");
		patientForm5.addComponents();
		patientForm5.setFrameFeatures();
		patientForm5.addListeners();
	}

	protected void addListeners() {
		super.addListeners();
		subMenuColorRed.addActionListener(this);
		subMenuColorBlue.addActionListener(this);
		subMenuColorGreen.addActionListener(this);
		subMenuColorOther.addActionListener(this);
		subMenuSize16.addActionListener(this);
		subMenuSize20.addActionListener(this);
		subMenuSize24.addActionListener(this);
		subMenuSizeOther.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object src = e.getSource();
		if (src == subMenuColorRed) {
			actionColor(Color.RED);
		} else if (src == subMenuColorGreen) {
			actionColor(Color.GREEN);
		} else if (src == subMenuColorBlue) {
			actionColor(Color.BLUE);
		} else if (src == subMenuSize16) {
			actionSize(16);
		} else if (src == subMenuSize20) {
			actionSize(20);
		} else if (src == subMenuSize24) {
			actionSize(24);
		}

	}

	private void actionSize(int size) {
		Font font = new Font(Font.SANS_SERIF, Font.BOLD, size);
		nameTextField.setFont(font);
		birthdateTextField.setFont(font);
		weightTextField.setFont(font);
		heightTextField.setFont(font);
		addressTextArea.setFont(font);
	}

	private void actionColor(Color color) {
		nameTextField.setForeground(color);
		birthdateTextField.setForeground(color);
		weightTextField.setForeground(color);
		heightTextField.setForeground(color);
		addressTextArea.setForeground(color);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
