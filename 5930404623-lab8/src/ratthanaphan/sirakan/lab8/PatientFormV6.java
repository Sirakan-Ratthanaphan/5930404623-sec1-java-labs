/**
 * PatientFormV6
 * @author Sirakan Ratthanaphan 593040462-3 sec1
 *         Created on 3/22/2017.
 */
package ratthanaphan.sirakan.lab8;

import java.awt.*;
import javax.swing.*;


public class PatientFormV6 extends PatientFormV5 {
	public PatientFormV6(String title) {
		super(title);
	}
	protected ImageIcon image = new ImageIcon("image/manee.jpg"); 
	protected JPanel newMainPanel; 

	public static void createAndShowGUI() {
		PatientFormV6 patientForm6 = new PatientFormV6("Patient Form V6");
		patientForm6.addComponents();
		patientForm6.setFrameFeatures();
		patientForm6.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		newMainPanel = new JPanel();
		newMainPanel.setLayout( new BorderLayout(0, 8));
		newMainPanel.add(new JLabel(image), BorderLayout.NORTH);
		newMainPanel.add(mainPanel,BorderLayout.SOUTH);
		openMenuItem.setIcon(new ImageIcon("image/openIcon.png"));
		saveMenuItem.setIcon(new ImageIcon("image/saveIcon.png"));
		exitMenuItem.setIcon(new ImageIcon("image/quitIcon.png"));
		this.add(newMainPanel);
	}
}
