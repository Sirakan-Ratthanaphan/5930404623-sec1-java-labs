/*
 * This program is to show the patient form window.
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/02/21
 */
package ratthanaphan.sirakan.lab6;

import java.awt.*;

import javax.swing.*;

public class PatientFormV2 extends PatientFormV1 {
	protected JPanel mainPanel, genderAndAddressPanel, genderPanel, boxOfGenderPanel, addressPanel;
	protected JLabel genderLabel, addressLabel;
	protected JRadioButton femaleCheckBox, maleCheckBox;
	protected JTextArea addressTextArea;
	protected JScrollPane addressJScrollPane;

	public PatientFormV2(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void createAndShowGUI() {
		PatientFormV2 patientForm1 = new PatientFormV2("Patient Form V2");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		super.addComponents();
		mainPanel = new JPanel(new BorderLayout());
		boxOfGenderPanel = new JPanel();
		genderPanel = new JPanel(new GridLayout(0, 2));
		genderAndAddressPanel = new JPanel(new BorderLayout());
		addressPanel = new JPanel(new BorderLayout());
		ButtonGroup choices = new ButtonGroup();
		maleCheckBox = new JRadioButton("Male");
		femaleCheckBox = new JRadioButton("Female");
		choices.add(maleCheckBox);
		choices.add(femaleCheckBox);
		boxOfGenderPanel.add(maleCheckBox);
		boxOfGenderPanel.add(femaleCheckBox);
		genderLabel = new JLabel("Gender:");
		genderPanel.add(genderLabel);
		genderPanel.add(boxOfGenderPanel);

		addressTextArea = new JTextArea(2, 35);
		addressTextArea.setLineWrap(true);
		addressTextArea.setWrapStyleWord(true);
		addressTextArea.setText(
				"Department of Computer Engineering, Faculty of Engineering, Khon Kaen University, Mittraparp Rd., T. Naimuang, A. Muang, Khon Kaen, Thailand, 40002");
		addressJScrollPane = new JScrollPane(addressTextArea);
		addressLabel = new JLabel("Address:");
		addressPanel.add(addressLabel, BorderLayout.NORTH);
		// addressPanel.add(addressTextArea);
		addressPanel.add(addressJScrollPane, BorderLayout.SOUTH);

		genderAndAddressPanel.add(genderPanel, BorderLayout.NORTH);
		genderAndAddressPanel.add(addressPanel, BorderLayout.CENTER);
		mainPanel.add(informationPanel, BorderLayout.NORTH);
		mainPanel.add(genderAndAddressPanel, BorderLayout.CENTER);
		mainPanel.add(panel, BorderLayout.SOUTH);
		add(mainPanel);
	}
}
