/*
 * This program is to show the patient form window.
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/02/21
 */
package ratthanaphan.sirakan.lab6;

import java.awt.*;

import javax.swing.*;

public class PatientFormV3 extends PatientFormV2 {
	private static final long serialVersionUID = 8212526250416860953L;
	protected JPanel mainPanel, typeOfPatientPanel;
	protected JLabel typeOfPatientLabel;
	protected JComboBox<String> typeOfPatientChoice;
	protected JMenuBar menuBar;
	protected JMenu fileMenu, configMenu;
	protected JMenuItem newMenuItem, openMenuItem, saveMenuItem, exitMenuItem, colorMenuItem, sizeMenuItem;

	public PatientFormV3(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void createAndShowGUI() {
		PatientFormV3 patientForm1 = new PatientFormV3("Patient Form V3");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		super.addComponents();
		mainPanel = new JPanel(new BorderLayout());
		typeOfPatientLabel = new JLabel("Type:");
		typeOfPatientPanel = new JPanel(new GridLayout(0, 2));
		typeOfPatientChoice = new JComboBox<String>();
		typeOfPatientChoice.addItem("Inpatient");
		typeOfPatientChoice.addItem("Outpatient");
		typeOfPatientChoice.setSelectedItem("Outpatient");
		typeOfPatientPanel.add(typeOfPatientLabel);
		typeOfPatientPanel.add(typeOfPatientChoice);

		genderAndAddressPanel.add(typeOfPatientPanel, BorderLayout.SOUTH);
		mainPanel.add(informationPanel, BorderLayout.NORTH);
		mainPanel.add(genderAndAddressPanel, BorderLayout.CENTER);
		mainPanel.add(panel, BorderLayout.SOUTH);
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		newMenuItem = new JMenuItem("New");
		openMenuItem = new JMenuItem("Open");
		saveMenuItem = new JMenuItem("Save");
		exitMenuItem = new JMenuItem("Exit");
		fileMenu.add(newMenuItem);
		fileMenu.add(openMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(exitMenuItem);
		configMenu = new JMenu("Config");
		colorMenuItem = new JMenuItem("Color");
		sizeMenuItem = new JMenuItem("Size");
		configMenu.add(colorMenuItem);
		configMenu.add(sizeMenuItem);
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		JMenuItem openMenuItem, saveMenuItem, exitMenuItem, colorMenuItem, sizeMenuItem;
		setJMenuBar(menuBar);
		add(mainPanel);
	}

}