package ratthanaphan.sirakan.lab5;

public interface HasInsurance {
	public void pay ();
	public void pay (double amount);
}
