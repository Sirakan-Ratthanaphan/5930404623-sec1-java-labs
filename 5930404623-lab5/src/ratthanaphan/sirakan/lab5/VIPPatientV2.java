package ratthanaphan.sirakan.lab5;

public class VIPPatientV2 extends PatientV3 implements HasInsurance, HasPet {
	private double totalDonation;
	private String passPhrase;
	
	public VIPPatientV2(String name, String birthdate, Gender gender, double weight, int height, double totalDonation,
			String passPhrase) {
		super(encrypt(name), birthdate, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;
	}

	public void donate(double donate) {
		totalDonation += donate;
	}

	private static String encrypt(String message) {
		int lenghtOfName = message.length();
		char[] stringToChar = message.toCharArray();
		for (int i = 0 ; i < lenghtOfName ; i++) {
			stringToChar[i] = (char)(stringToChar[i] + 7);
			 if (stringToChar[i] > 'z') {
				 stringToChar[i] = (char)(stringToChar[i] - 26);
			 }
		}
		message = String.valueOf(stringToChar);
		return message;
	}
	private static String decrypt(String message) {
		int lenghtOfName = message.length();
		char[] stringToChar = message.toCharArray();
		for (int i = 0 ; i < lenghtOfName ; i++) {
			stringToChar[i] = (char)(stringToChar[i] - 7);
			 if (stringToChar[i] < 'a') {
				 stringToChar[i] = (char)(stringToChar[i] + 26);
			 }
		}
		message = String.valueOf(stringToChar);
		return message;
	}

	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDanation) {
		this.totalDonation = totalDanation;
	}

	public String getVIPName(String passPhrase) {
		if (passPhrase.equals(this.passPhrase)) {
			return decrypt(this.getName());
		} else
			return "Wrong passphrase, please try again";
	}

	@Override
	public void setName(String name) {
		super.setName(name);
	}

	@Override
	public String toString() {
		return "VIPPatient [" + totalDonation + ", " + super.toString();
	}

	public void pay() {

	}

	public void pay(double amount) {

	}

	public void feedPet() {
		System.out.println("feed the pet.");
	}

	public void playWithPet() {
		System.out.println("play with pet.");
	}

}
