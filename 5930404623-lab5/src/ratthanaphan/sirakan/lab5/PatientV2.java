package ratthanaphan.sirakan.lab5;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import ratthanaphan.sirakan.lab4.Patient;

import java.time.temporal.ChronoUnit;

public class PatientV2 extends Patient {

	public PatientV2(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);
		// TODO Auto-generated constructor stub
	}
	public void patientReport()
    {
         System.out.println ("You need medical attention.");
    }
}
