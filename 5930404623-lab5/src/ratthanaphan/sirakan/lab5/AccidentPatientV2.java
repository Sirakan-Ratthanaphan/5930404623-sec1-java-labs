package ratthanaphan.sirakan.lab5;

public class AccidentPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {
	public AccidentPatientV2(String name, String birthdate, Gender gender, double weight, int height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthdate, gender, weight, height);
	}
	public void pay() {
		System.out.println("pay the accident bill with insurrance");
	}

	public void pay(double amount) {

	}

	public void askPermission() {
		if (this.getGender() == Gender.MALE) {
			System.out.println("ask parents for premission to cure him in accident.");
		} else {
			System.out.println("ask parents for premission to cure her in accident.");
		}
	}
	public void askPermission(String legalGuardianName) {

	}
	public void seeDoctor() {
		// TODO Auto-generated method stub
		
	}
	
}
