package ratthanaphan.sirakan.lab5;

public class VIPPatient extends PatientV2 {

	public VIPPatient(String name, String birthdate, Gender gender, double weight, int height,double totalDonation,String passPhrase) {
		super(name, birthdate, gender, weight, height);
		// TODO Auto-generated constructor stub
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;
	}
	public String toString() {
		return "VIPPatient ["+getTotalDonation()+"], Patient ["+getName()+", "+getBirthdate()+", "+getGender()+ ", " + getWeight() + " kg., " + getHeight() + " cm.";

	}
	private static double totalDonation;
	private String passPhrase;
	public void setTotalDonation(double totalDonation) {
		VIPPatient.totalDonation = totalDonation;
	}
	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}
	public double getTotalDonation()
	{
		return totalDonation;
	}
	@Override
	public void patientReport() {
		// TODO Auto-generated method stub
		System.out.println("Your record is private.");
	}
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		super.setName(name);
	}
	public String getName() {
		//return encrypt(super.getName());
		return super.getName();
		//return "AccidentPatient ["+getTypeOfAccident()+getIsInICU()+"]";
		
	}
	public void setVIPName(String name) {
		// TODO Auto-generated method stub
		super.setName(name);
	}
	public String getVIPName(String password) {
		if(password.equals(passPhrase))
			return decrypt(encrypt(super.getName()));
		else
			return "Wrong passphrase, please try again";
		//return "AccidentPatient ["+getTypeOfAccident()+getIsInICU()+"]";
		
	}
	static void donate (double donate)
	{
		totalDonation=totalDonation+donate;
	}
	private static String encrypt(String message) {
		int numberOfLetter= message.length();
		char[] stringToCharArray=message.toCharArray();
		for(int i = 0 ;i<numberOfLetter;i++)
		{
			stringToCharArray[i]=(char) (stringToCharArray[i]+7);
			if(stringToCharArray[i]>'z'){
				stringToCharArray[i]=(char) (stringToCharArray[i]-97);
			}
		}
		message=String.valueOf(stringToCharArray);
		return message;
	}
	private static String decrypt(String message) {
		int numberOfLetter= message.length();
		char[] stringToCharArray=message.toCharArray();
		for(int i = 0 ;i<numberOfLetter;i++)
		{
			stringToCharArray[i]=(char) (stringToCharArray[i]-7);
			if(stringToCharArray[i]<'a')	{
				stringToCharArray[i]=(char) (stringToCharArray[i]+65);
			}
		}
		message=String.valueOf(stringToCharArray);
		return message;
	}
}
