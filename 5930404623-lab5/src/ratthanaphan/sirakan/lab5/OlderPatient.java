package ratthanaphan.sirakan.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import ratthanaphan.sirakan.lab4.Patient.Gender;


public class OlderPatient {
	public static void main(String[] str) {
		PatientV2 piti = new AccidentPatient("piti","12.01.2000", Gender.MALE ,65.5,169,"Car accident",true);
		PatientV2 weera = new TerminalPatient("weera","15.02.2000", Gender.MALE ,72,172,"Cancer","01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000, "mickeymouse");
		AccidentPatient petch = new AccidentPatient("petch","15.10.1999", Gender.MALE ,68,170,"Fire accident",true);;
		isOlder(piti,weera);
		isOlder(piti,duangjai);
		isOlder(piti,petch);
		isOldest(piti,weera,duangjai);
		isOldest(piti,duangjai,petch);
	}
	
	public static void isOlder(PatientV2 patient1, PatientV2 patient2) {
		DateTimeFormatter newTimeFormatter = DateTimeFormatter.ofLocalizedDate(
		        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		LocalDate dateBefore= patient1.getBirthdate();
		LocalDate dateAfter= patient2.getBirthdate();
		long daysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
		if(daysBetween>0){
				System.out.println(patient1.getName()+" is older than "+patient2.getName()+".");
			}else {
				System.out.println(patient1.getName()+" is not older than "+patient2.getName()+".");
			}
	}
	public static void isOldest(PatientV2 patient1, PatientV2 patient2,PatientV2 patient3) {
		DateTimeFormatter newTimeFormatter = DateTimeFormatter.ofLocalizedDate(
		        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		LocalDate date1= patient1.getBirthdate();
		LocalDate date2= patient2.getBirthdate();
		LocalDate date3= patient3.getBirthdate();
		long daysBetween1 = ChronoUnit.DAYS.between(date1, date2);
		long daysBetween2 = ChronoUnit.DAYS.between(date1, date3);
		long daysBetween3 = ChronoUnit.DAYS.between(date2, date3);
		if(daysBetween1>0 && daysBetween2>0){
				System.out.println(patient1.getName()+" is the oldest among "+patient1.getName()+", "+patient2.getName()+" and "+patient3.getName()+".");
			}else if(daysBetween2>0) {
				System.out.println(patient2.getName()+" is the oldest among "+patient1.getName()+", "+patient2.getName()+" and "+patient3.getName()+".");
			}else {
				System.out.println(patient3.getName()+" is the oldest among "+patient1.getName()+", "+patient2.getName()+" and "+patient3.getName()+".");
			}
	}
}
