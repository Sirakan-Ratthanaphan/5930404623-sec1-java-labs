package ratthanaphan.sirakan.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.EnumSet;
import java.time.temporal.ChronoUnit;
public class TerminalPatient extends PatientV2 {
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	public TerminalPatient(String name, String birthdate, Gender gender, double weight, int height, String terminalDisease, String firstDiagnosed) {
		super(name, birthdate, gender, weight, height);
		// TODO Auto-generated constructor stub
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed,germanFormatter);
		this.terminalDisease = terminalDisease;
	}
	private String terminalDisease;
	private LocalDate firstDiagnosed;
	DateTimeFormatter newTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);
	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}
	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease=terminalDisease;
	}
	public String getTerminalDisease(){
		return terminalDisease;
	}
	public LocalDate getFirstDiagnosed(){
		return firstDiagnosed;
	}
	public String toString() {
		return "TerminalPatient ["+getTerminalDisease()+", "+getFirstDiagnosed()+"], Patient ["+getName()+", "+getBirthdate()+", "+getGender()+ ", " + getWeight() + " kg., " + getHeight() + " cm.";

	}
	@Override
	public void patientReport() {
		// TODO Auto-generated method stub
		System.out.println("You have terminal illness.");
	}
	
}
