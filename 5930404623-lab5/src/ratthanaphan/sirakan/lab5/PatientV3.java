package ratthanaphan.sirakan.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import ratthanaphan.sirakan.lab5.PatientV3.Gender;

public class PatientV3 {

	private String name;
	private LocalDate birthdate;

	public static enum Gender {
		MALE, FEMALE
	};
	private Gender gender;
	private double weight;
	private int height;
	DateTimeFormatter newTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);
	public String getGender;

	public void setName(String name) {
		this.name = name;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Patient [" + getName() + ", " + getBirthdate() + ", " + getGender() + ", " + getWeight() + " kg., "
				+ getHeight() + " cm.]";
	}

	public PatientV3(String name, String birthdate, Gender gender, double weight, int height) {
		super();
		this.name = name;
		this.birthdate = LocalDate.parse(birthdate, newTimeFormatter);
		this.gender = gender;
		this.weight = weight;
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public PatientV3.Gender getGender() {
		return gender;
	}

	public double getWeight() {
		return weight;
	}

	public int getHeight() {
		return height;
	}

}
