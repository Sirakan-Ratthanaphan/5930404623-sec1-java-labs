package ratthanaphan.sirakan.lab5;

public class AccidentPatient extends PatientV2{

	public AccidentPatient(String name, String birthdate, Gender gender, double weight, int height, String typeOfAccident, boolean isInICU) {
		super(name, birthdate, gender, weight, height);
		this.typeOfAccident=typeOfAccident;
		this.isInICU = isInICU;
		// TODO Auto-generated constructor stub
	}
	private String typeOfAccident;
	private boolean isInICU;
	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}
	public void setIsInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}
	
	public String toString() {
		//return "AccidentPatient ["+getTypeOfAccident()+getIsInICU()+"]";
		if(getIsInICU()==true)
			return "AccidentPatient ["+getTypeOfAccident()+", In ICU], Patient ["+getName()+", "+getBirthdate()+", "+getGender()+", " + getWeight() + " kg., " + getHeight() + " cm.";
		else
			return "AccidentPatient ["+getTypeOfAccident()+", Out ICU], Patient ["+getName()+", "+getBirthdate()+", "+getGender()+ ", " + getWeight() + " kg., " + getHeight() + " cm.";
	}
	public String getTypeOfAccident()
	{
		return typeOfAccident;
	}
	public boolean getIsInICU() {
		return isInICU;
	}
	
	public void patientReport()
    {
         System.out.println ("You were in an accident.");
    }
	
}
