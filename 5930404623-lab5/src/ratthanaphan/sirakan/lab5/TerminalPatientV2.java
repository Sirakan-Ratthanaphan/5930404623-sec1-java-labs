package ratthanaphan.sirakan.lab5;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.time.LocalDate;

public class TerminalPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {

	private String terminalDisease;
	private LocalDate firstDiagnosed;

	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	public TerminalPatientV2(String name, String birthdate, Gender gender, double weight, int height,
			String terminalDisease, String firstDiagnosed) {
		super(name, birthdate, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed, germanFormatter);
	}

	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}

	@Override
	public String toString() {

		return "TerminalPatient [" + terminalDisease + firstDiagnosed + "], " + super.toString();
	}

	public void pay() {

	}

	public void pay(double amount) {
		if (this.getGender() == Gender.MALE) {
			System.out.println("pay " + amount + " baht for his hospital visit by insurrance.");
		} else {
			System.out.println("pay " + amount + " baht for her hospital visit by insurrance.");
		}
	}

	public void askPermission() {

	}

	public void askPermission(String legalGuardianName) {
		System.out.println("ask " + legalGuardianName + " for premission to treat with Chemo.");
	}

	public void seeDoctor() {
		// TODO Auto-generated method stub
		
	}

}