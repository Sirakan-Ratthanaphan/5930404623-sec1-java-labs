package ratthanaphan.sirakan.lab5;

public interface HasPet {
	public void feedPet();
	public void playWithPet();
}