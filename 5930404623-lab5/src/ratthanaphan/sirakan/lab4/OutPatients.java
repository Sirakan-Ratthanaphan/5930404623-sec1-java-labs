package ratthanaphan.sirakan.lab4;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.time.temporal.ChronoUnit;

import ratthanaphan.sirakan.lab4.Patient.Gender;

import java.util.EnumSet;
public class OutPatients extends Patient {
	
	//public static String hospitalName;
	public OutPatients(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);
	}
	public OutPatients(String name, String birthdate, Gender gender, double weight, int height, String visitdate) {
		super(name, birthdate, gender, weight, height);
		this.visitdate = visitdate;
		
	}
	private String visitdate;
	@Override
	public String toString() {
		//return "OutPatients [getVisitdate()=" + getVisitdate() + "]";
		return "OutPatient [" + getName() + ", birthdate = " + getBirthdate() + ", " + getGender()
		+ ", " + getWeight() + " kg., " + getHeight() + " cm. visit date = " +getVisitdate()+"]";
	}
	static String hospitalName = "Srinakarin";
	//private String hospitalname;
	//public static void hospitalName() {
		//System.out.print("Srinakarin");
		//hospitalName="Srinakarin";
	//}
	
	public LocalDate getVisitdate() {
		DateTimeFormatter newTimeFormatter = DateTimeFormatter.ofLocalizedDate(
		        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		    LocalDate newFormatterVisitdate = LocalDate.parse(visitdate, newTimeFormatter);
		return newFormatterVisitdate;
	}
	public void setVisitdate(String visitdate) {
		this.visitdate = visitdate;
		
	}
	public long displayDaysBetween(OutPatients outPatients) {
		DateTimeFormatter newTimeFormatter = DateTimeFormatter.ofLocalizedDate(
		        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		LocalDate dateBefore= LocalDate.parse(visitdate, newTimeFormatter);
		LocalDate dateAfter= LocalDate.parse(outPatients.visitdate, newTimeFormatter);
		long daysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
		if(daysBetween<0){
			daysBetween=0-daysBetween;
			System.out.println(getName()+" visited after "+outPatients.getName()+" for "+daysBetween+" Days.");
		}
		else {
			System.out.println(outPatients.getName()+" visited after "+getName()+" for "+daysBetween+" Days.");
		}
		return daysBetween;
	}
	
}



