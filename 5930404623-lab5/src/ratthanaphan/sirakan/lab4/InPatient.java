package ratthanaphan.sirakan.lab4;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.time.temporal.ChronoUnit;

import ratthanaphan.sirakan.lab4.Patient.Gender;

import java.util.EnumSet;
public class InPatient extends Patient {
	
	private LocalDate admitDate ; 
	private LocalDate dischargeDate ;
	
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	public InPatient(String name, String birthdate, Gender gender, double weight, int height,String admitDate,String dischargeDate) {
		super(name, birthdate, gender, weight, height);
		this.admitDate = LocalDate.parse(admitDate,germanFormatter);
		this.dischargeDate = LocalDate.parse(dischargeDate,germanFormatter); 
		
	} 
	@Override 
	public String toString() {
		return "InPatient [" + getName() + ", " + super.getBirthdate() + ", " + super.getGender() + ", " + super.getWeight() + " kg., " + super.getHeight() + " cm." +" admitDate=" + admitDate + ", "+"dischargeDate="+ dischargeDate + "]" ;
	}
	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate  = LocalDate.parse(dischargeDate,germanFormatter);
	}
	public LocalDate getDischargeDate() {
		return dischargeDate;
	}
	public void setDischargeDate(LocalDate dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	public void setAdmitDate(String admitDate) {
		this.admitDate = LocalDate.parse(admitDate,germanFormatter);
	}
	public LocalDate getAdmitDate() {
	return admitDate;
}
	public void setAdmitDate(LocalDate admitDate) {
	this.admitDate = admitDate;
}
	
}
