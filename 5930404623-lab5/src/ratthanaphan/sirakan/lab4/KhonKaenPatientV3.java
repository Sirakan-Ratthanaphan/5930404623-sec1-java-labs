package ratthanaphan.sirakan.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.time.temporal.ChronoUnit;

import ratthanaphan.sirakan.lab4.Patient.Gender;

/**
 * KhonKaenPatients
 * 
 * This program is to illustrate the examples of how to create objects in class Patient
 * 
 * @author Sirakan Ratthanaphan
 * 
 * 9-02-2017
 *
 *
 *
 */


public class KhonKaenPatientV3 {
	public static void main(String[] args) {
		Patient manee = new InPatient("Manee", "01.12.1980",Gender.FEMALE, 60, 150 ,"20.01.2017","29.01.2017");
		Patient mana = new OutPatients("Mana", "22.04.1981",Gender.MALE, 70, 160, "23.01.2017");
		Patient chujai = new Patient("Chujai", "03.03.1980",Gender.FEMALE, 41.5, 175);
		InPatient piti = new InPatient("Piti", "05.05.1980",Gender.MALE,65,165,"11.01.2017","17.01.2017");
		System.out.println(manee);
		System.out.println(mana);
		System.out.println(chujai);
		System.out.println(piti);
		InPatient manee_new = (InPatient)manee;
		manee_new.setDischargeDate("31.01.2017");
		System.out.println("The new discharged date for " + manee_new.getName()+" is "+manee_new.getDischargeDate()+".");
		OutPatients mana_new = (OutPatients)mana;
		mana_new.setVisitdate("14.02.2017");
		System.out.println("The new visit date for "+mana_new.getName()+" is "+mana_new.getVisitdate()+".");
		piti.setAdmitDate("15.01.2017");
		System.out.println("The new admit date for "+piti.getName()+" is "+piti.getAdmitDate()+".");
	}

	/*private static boolean isTaller(Patient manee, Patient mana) {
		if(manee.getHeight() > mana.getHeight()){
				return true ;
			}else {
				return false ;
			}
	}*/

}
