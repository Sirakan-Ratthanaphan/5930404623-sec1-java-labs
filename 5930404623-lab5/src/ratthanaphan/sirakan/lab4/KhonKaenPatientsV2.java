package ratthanaphan.sirakan.lab4;
/**
 * KhonKaenPatients
 * 
 * This program is to illustrate the examples of how to create objects in class Patient
 * 
 * @author Sirakan Ratthanaphan
 * 
 * 9-02-2017
 *
 *
 *
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ratthanaphan.sirakan.lab4.Patient.Gender;

import java.time.temporal.ChronoUnit;
public class KhonKaenPatientsV2 {
		public static void main(String[] args){
			OutPatients chujai = new OutPatients("Chujai" , "02.06.1995", Gender.FEMALE, 52.7,150 );
			OutPatients piti = new OutPatients("Piti" , "13.08.1995", Gender.MALE, 52.7,150,"21.01.2017" );
			chujai.setVisitdate("25.01.2017");
			System.out.println(chujai);
			System.out.println(piti);
			chujai.displayDaysBetween(piti);
			System.out.println("Both of them went to " + OutPatients.hospitalName + " hospital.");
		}
}
