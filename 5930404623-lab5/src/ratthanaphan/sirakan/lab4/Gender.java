package ratthanaphan.sirakan.lab4;

//enum that contains gender of patients
public enum Gender {
	MALE, FEMALE
}