/*
 * This program is guess the number game.
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/01/24
 */
package ratthanaphan.sirakan.lab3;

import java.util.Scanner;

public class GuessNumberGame {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int remainingGuess = 7;
		int answer = (int) (Math.random() * 101);
		for (int i = remainingGuess; i > 0; i--) {
			System.out.println("Number of remaining guess is " + i);
			System.out.print("Enter a guess: ");
			int guessnumber = in.nextInt();
			if (guessnumber == answer) {
				System.out.println("Correct!");
				break;
			} else if (guessnumber > answer) {
				System.out.println("Higher!");
			} else {
				System.out.println("Lower!");
			}
			if (i == 1) {
				System.out.print("You ran out of guesses. The number was " + answer);
			}

		}
	}
}
