/*
 * This program is guess the number game.
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/01/24
 */
package ratthanaphan.sirakan.lab3;

import java.util.Scanner;

public class GuessNumberGameV2 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Scanner instring = new Scanner(System.in);

		// start:
		int nextplay = 1;
		for (int j = 0; nextplay != -1; j++) {

			System.out.print("Enter min and max of random number: ");
			int min = in.nextInt();
			int max = in.nextInt();
			int swap;
			if (min > max) {
				swap = min;
				min = max;
				max = swap;
			}
			System.out.print("Enter the number possible guess : ");
			int numberpossibleguess = in.nextInt();
			int answer = min + (int) (Math.random() * ((max - min) + 1));
			for (int i = numberpossibleguess; i > 0; i--) {
				System.out.println("Number of remaining guess is " + i);
				System.out.print("Enter a guess: ");
				int guessnumber = in.nextInt();
				if (guessnumber > max || guessnumber < min) {
					System.out.println("Numbers must be in " + min + " and " + max);
					i++;
					continue;
				}
				if (guessnumber == answer) {
					System.out.println("Correct!");
					System.out.print("Want to play again? (\"Y\" or \"y\") : ");
					String playagain = instring.nextLine();
					if (playagain.equalsIgnoreCase("y")) {
					} else {
						nextplay = -1;
						System.out.println("End game");
					}
					break;
				} else if (guessnumber > answer) {
					System.out.println("Higher!");
				} else {
					System.out.println("Lower!");
				}
				if (i == 1) {
					System.out.println("You ran out of guesses. The number was " + answer);
					System.out.print("Want to play again? (\"Y\" or \"y\") : ");
					String playagain = instring.nextLine();
					if (playagain.equalsIgnoreCase("y")) {
					} else {
						nextplay = -1;
						System.out.println("End game");
					}
					break;
				}

			}
		}
	}
}
