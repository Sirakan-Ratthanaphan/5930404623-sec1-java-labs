/*
 * This program is to show the simple window.
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/02/21
 */
package ratthanaphan.sirakan.lab6;

import java.awt.*;

import javax.swing.*;

public class MySimpleWindow extends JFrame {
	JPanel panel;
	JButton cancelButton;
	JButton OKButton;

	public MySimpleWindow(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	protected void addComponents() {
		panel = new JPanel();
		cancelButton = new JButton("Cancel");
		OKButton = new JButton("OK");
		panel.add(cancelButton);
		panel.add(OKButton);
		this.add(panel);
	}

}
