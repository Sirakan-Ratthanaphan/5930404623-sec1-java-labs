/*
 * This program is to show the patient form window.
 * 
 * It is written by Sirakan Ratthanaphan
 * 2017/02/21
 */
package ratthanaphan.sirakan.lab6;

import java.awt.*;

import javax.swing.*;

public class PatientFormV1 extends MySimpleWindow {
	JPanel mainPanel, informationPanel;
	JLabel nameLabel;
	JTextField nameTextField;
	JLabel birthdateLabel;
	JTextField birthdateTextField;
	JLabel weightLabel;
	JTextField weightTextField;
	JLabel heightLabel;
	JTextField heightTextField;

	public PatientFormV1(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		super.addComponents();
		mainPanel = new JPanel(new BorderLayout());
		informationPanel = new JPanel(new GridLayout(0, 2));
		nameLabel = new JLabel("Name:");
		nameTextField = new JTextField(15);
		informationPanel.add(nameLabel);
		informationPanel.add(nameTextField);
		birthdateLabel = new JLabel("Birthdate:");
		birthdateTextField = new JTextField(15);
		informationPanel.add(birthdateLabel);
		informationPanel.add(birthdateTextField);
		weightLabel = new JLabel("Weight (kg.):");
		weightTextField = new JTextField(15);
		informationPanel.add(weightLabel);
		informationPanel.add(weightTextField);
		heightLabel = new JLabel("Height (metre):");
		heightTextField = new JTextField(15);
		informationPanel.add(heightLabel);
		informationPanel.add(heightTextField);
		mainPanel.add(informationPanel, BorderLayout.NORTH);
		mainPanel.add(panel, BorderLayout.SOUTH);
		this.add(mainPanel);
	}

}
